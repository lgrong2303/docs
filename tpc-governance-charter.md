![TPC charter 总览](./imgs/charter1.png)

# 1 TPC 的愿景与使命

构建OpenHarmony 北向应用三方库的可持续发展生态，打造 OpenHarmony TPC（Third Party Component）项目的孵化器和优选平台：

1. 持续发掘和孵化具备发展潜力的 TPC 项目及其团队，为其项目成长提供 TPC 治理方案和工程方案的辅导和支撑；
2. 优选高质量 TPC 以及其团队，使用发行版持续迭代每个技术垂域的当前最优解。 
# 2 角色与职责

## 2.1 TPC SIG Lead

TPC SIG Lead是TPC SIG的治理团队，负责所有TPC项目的技术方向、成熟度级别审核和项目健康监督。

## 2.2 TPC TAG

TAG（Technical Advisory Group）是指具有特定TPC知识领域的专家组成的小组，就TPC-SIG中具体的项目给出技术指导和咨询意见。

## 2.3 TPC 垂域 Commiter

TPC 垂域Commiter是某一技术垂域的Commiter，具有相关技术垂域项目的管理权限。

## 2.4 TPC 项目 Commiter

TPC 项目Commiter是某一项目的Commiter，具有相关项目的管理权限。

# 3 TPC 项目成熟度级别

根据TPC在APP中的采用规模、开源项目的健康度，我们将TPC项目按成熟度分为三个级别，沙箱项目（Sandbox）、孵化项目（Incubating）、毕业项目（Graduated）。

## 3.1 沙箱项目

Sandbox项目处于技术验证阶段，未被APP实际依赖。 

**注：**沙箱项目是尚未被OpenHarmony社区正式接纳的TPC项目，TPC SIG从建立三方库生态角度出发，为其提供技术能力支撑，完成技术验证，促进其成为孵化项目。 

### 3.1.1 目标

1. 我们鼓励项目获得及时的技术验证和开发者的贡献反馈， 判断其成为OpenHarmoy TPC生态的可能性。
2. 判断其与现有TPC项目的差异化竞争力，及与OpenHarmony平台功能的兼容性和一致性。 
3. 为其提供发展所必须的服务。
4. 通过确保所有项目遵守OpenHarmony社区的法律、行为准则和知识产权政策要求，消除可能存在的法律和治理障碍，以便推动采纳和贡献。
### 3.1.2 协作流程

![沙箱流程图](./imgs/charter2.png)

1. **沙箱申请**：
    1. 如果项目已经建仓，可以直接建立对本项目的沙箱申请报告以及Issue（TODO）, 申请成为Sandbox项目。 
    2. 如果项目尚无正式建仓，仍然可以成为沙箱项目：
        1. 根据具体诉求，建立三方库适配候选方案，就候选三方库建立[沙箱选型报告](https://gitee.com/openharmony-tpc/docs/blob/master/tpc-governance-platform-user-guide.md)。 
        2. 对选型报告内容进行风险评估后，[创建Issue](https://gitee.com/openharmony-tpc/docs/blob/master/tpc-governance-platform-user-guide.md), 申请成为Sandbox项目。
2. **沙箱评审**：TPC-SIG Lead及 垂域Commiter 将根据孵化评审申请Issue，及相关评估报告, 集体决策其是否成为 Incubating项目。决策优先依托Issue。如有未决策事项，可以上升到SIG会议上进行评审。  
3. **沙箱建仓**：对于沙箱项目，尚未被OpenHarmony社区正式接纳，所以不能在官方代码平台建仓，但是TPC SIG会提供相应建仓指导（TODO）在外部建仓。
4. **沙箱认证项目**：通过沙箱评审项目，将获得沙箱徽章认证（TODO）（该徽章会明确声明：该项目尚未被OpenHarmony社区正式接纳），可在项目中使用。项目同时会在TPC 沙箱项目LandScape呈现。
5. **沙箱项目治理**：沙箱项目将在TPC 孵化治理看板（TODO）呈现，并进行日常治理。 
6. **开发阶段**：参考开发指南（TODO）
7. **归档评审:** TPC-SIG Lead 将周期性审视沙箱项目，根据相关归档评估指标（TODO）,对相关项目做归档处理。 
### 3.1.3 服务

我们对Sandbox提供的服务包含：

1. 开源项目的治理指导（TODO）。 
2. 法务服务（TODO）。
3. 用户社区链接。 （TODO）
4. 市场营销活动（TODO）。
5. 合规及漏洞安全支撑。(TODO) 
6. 工具集合。（TODO）
## 3.2 孵化项目

Incubating项目已经被部分APP实际依赖使用，并有稳定的贡献者群体。

### 3.2.1 目标

1. 通过成为孵化项目及时得到市场反馈和贡献者的持续贡献。 
2. 增加APP采用量、扩大社区规模、提高项目质量和稳定性，促使其成为毕业项目。 
### 3.2.2 协作流程

![孵化流程图](./imgs/charter3.png)


1. **孵化申请**：
    1. 如果项目在OpenHarmony社区以外已经建仓，同时没有将代码仓迁移到[OH官方代码平台](https://gitee.com/openharmony-sig)的诉求，可以直接建立对本项目的孵化申请报告以及Issue（TODO）, 申请成为Incubating项目。**注**：项目必须已经被至少一个鸿蒙单框架APP实际依赖使用，才可申请成为孵化项目。 
    2. 如果项目在OpenHarmony社区以外尚无正式建仓，但明确是具体鸿蒙单框架APP提出的具体诉求，仍然可以成为孵化项目：
        1. 根据具体诉求，建立三方库适配候选方案，就候选三方库建立[孵化选型报告](https://gitee.com/openharmony-tpc/docs/blob/master/tpc-governance-platform-user-guide.md)。 
        2. 对选型报告内容进行风险评估后，[创建Issue](https://gitee.com/openharmony-tpc/docs/blob/master/tpc-governance-platform-user-guide.md), 申请成为Incubating项目。
2. **孵化评审**：TPC-SIG Lead及 垂域Commiter 将根据孵化评审申请Issue，及相关评估报告, 集体决策其是否成为 Incubating项目。决策优先依托Issue。如有未决策事项，可以上升到SIG会议上进行评审。  
3. **OH架构评审**：TPC-SIG 相关代表（Lead代表、垂域及项目Commiter代表，各一名） 在OH 架构SIG会议中，陈述其成为OH 孵化项目的原因，并获取架构SIG的评审意见。 
4. **孵化建仓**：对于在OH-SIG建仓项目，将按要求建立（TODO）, 建立项目。
5. **孵化认证项目**：通过孵化评审项目，将获得孵化徽章认证（TODO），可在项目中使用。项目同时会在TPC 孵化项目LandScape呈现。
6. **孵化项目治理**：孵化项目将在TPC 孵化治理看板（TODO）呈现，并进行日常治理。 
7. **开发阶段**：参考开发指南（TODO）
8. **归档评审:** TPC-SIG Lead 将周期性审视孵化项目，根据相关归档评估指标（TODO）,对相关项目做归档处理。 
### 3.2.3 服务

1. 开源项目的治理指导（TODO）。 
2. 法务服务（TODO）。
3. 用户社区链接。（TODO）
4. 市场营销活动（TODO）。
5. 合规及漏洞安全支撑。(TODO) 
6. 工具集合。（TODO）
## 3.3 毕业项目

Graduated项目被视为功能完整、性能稳定，广泛被APP实际依赖使用，并有稳定的、较多的贡献者群体。

### 3.3.1 目标

1. 保持其技术领先地位， 持续增加用户和贡献者、确保项目的稳定性和可持续性，推动整个 TPC 生态系统的发展。 
### 3.3.2 协作流程

![毕业流程图](./imgs/charter4.png)


1. **毕业申请**：
    1. 如果项目在OpenHarmony社区以外已经建仓，同时没有将代码仓迁移到[OH官方代码平台](https://gitee.com/openharmony-tpc)的诉求，可以直接建立对本项目的毕业申请报告以及Issue（TODO）, 申请成为Graduated项目。 
    2. 如果项目已在OpenHarmony[社区官方代码平台](https://gitee.com/openharmony-sig)进行孵化，并希望在继续在社区官方平台进行毕业项目治理，可以可以直接建立对本项目的毕业申请报告以及Issue（TODO）, 申请成为Graduated项目。
2. **毕业评审**：TPC-SIG Lead及 垂域Commiter 将根据毕业评审申请Issue，及相关评估报告, 集体决策其是否成为Graduated项目。决策优先依托Issue。如有未决策事项，可以上升到SIG会议上进行评审。  
3. **OH架构评审**：TPC-SIG 相关代表（Lead代表、垂域及项目Commiter代表，各一名） 在OH 架构SIG会议中，陈述其成为OH 毕业项目的原因，并获取架构SIG的评审意见。 
4. **TPC建仓**：对于在[OH-TPC](https://gitee.com/openharmony-tpc)申请建仓项目，将按要求建立（TODO）项目。
5. **毕业认证项目**：通过毕业评审项目，将获得毕业徽章认证（TODO），可在项目中使用。项目同时会在TPC毕业项目LandScape呈现。
6. **毕业项目治理**：毕业项目将在TPC毕业治理看板（TODO）呈现，并进行日常治理。 
7. **开发阶段**：参考开发指南（TODO）
8. **归档评审:** TPC-SIG Lead 将周期性审视毕业项目，根据相关归档评估指标（TODO）,对相关项目做归档处理。 
### 3.3.3 服务

1. 开源项目的治理指导（TODO）。 
2. 法务服务（TODO）。
3. 用户社区链接。 （TODO）
4. 市场营销活动（TODO）。
5. 合规及漏洞安全支撑。(TODO) 
6. 工具集合。（TODO）
# 4 开发指南

## 4.1 目标

TODO

## 4.2 协作流程

![开发流程图](./imgs/charter5.png)


TODO

## 4.3 服务

TODO

# 5 发行版发布与维护

## 5.1 目标

使用发行版持续迭代每个技术垂域的当前最优解，提供生命周期的维护保障。 

## 5.2 协作流程

![发行版流程图](./imgs/charter6.png)


1. **发行版规划:** 定制发行版版本规划（TODO）
2. **发行版申请**: 创建Issue（TODO）, 根据模版要求申请成为发行版软件（TODO）
3. **发行版评审**: TPC-SIG Lead及 TAG将根据评审申请Issue，及相关评估报告（TODO）, 集体决策其是否成为发行版软件。
4. **发行版发布**：发行版软件将在TPC 发行版 LandScape（TODO）呈现。
5. **发行版认证项目：**通过发行版评审的项目，将获得发行版徽章认证（TODO），可在项目中使用
6. **采用度与口碑分析：**TODO
7. **漏洞与投毒感知:**TODO
8. **发行版维护:**TODO
9. **发行版EOL:**TPC-SIG Lead 根据发行版生命周期管理（TODO）,制定其EOL策略。
# 6 贡献评估

## 6.1 贡献量评估

TODO

## 6.2 成熟度级别认证

TODO

