# ImageKnife性能总结-网络优化

## 1、背景
加载过程中发现网络下载耗时长。

## 2、原因分析
1、使用downloadFile接口：使用该接口流程为：下载网络文件 → 网络数据写入本地磁盘 → 读取本地磁盘文件 → 写缓存 → 解码 → xxx → 显示图片。

2、如上流程，可分析出，使用downloadFile接口存在读写磁盘操作存在不必要的时间损耗。

## 3、修改方案
1、使用requestInStream接口： 该接口流程为：下载网络文件 → 写缓存 → 解码 → xxx → 显示图片。

代码参考：

```shell
import { IDataFetch } from '../networkmanage/IDataFetch'
import { RequestOption } from '../RequestOption'
import http from '@ohos.net.http'

// 数据加载器
class RequestData {
  receiveSize: number = 2000
  totalSize: number = 2000
}

export class HttpDownloadClient implements IDataFetch {
  async loadData(request: RequestOption, onComplete: (img: ArrayBuffer) => void, onError: (err: string) => void) {
    try {
      let httpRequest = http.createHttp()
      let arrayBuffers = new Array<ArrayBuffer>();
      httpRequest.on('headersReceive', (header: Object) => {
        // 跟服务器连接成功准备下载
        if (request.progressFunc) {
          // 进度条为0
          request.progressFunc.asyncSuccess(0)
        }
      })
      httpRequest.on('dataReceive', (data: ArrayBuffer) => {
        // 下载数据流多次返回
        arrayBuffers.push(data);
      })
      httpRequest.on('dataReceiveProgress', (data: RequestData) => {
        // 下载进度
        if (data != undefined && (typeof data.receiveSize == 'number') && (typeof data.totalSize == 'number')) {
          let percent = Math.round(((data.receiveSize * 1.0) / (data.totalSize * 1.0)) * 100)
          if (request.progressFunc) {
            request.progressFunc.asyncSuccess(percent)
          }
        }
      })
      httpRequest.on('dataEnd', () => {
        // 下载完毕
        let combineArray = this.combineArrayBuffers(arrayBuffers);
        onComplete(combineArray)
      })

      const headerObj: Record<string, Object> = {}
      request.headers.forEach((value, key) => {
        headerObj[key] = value
      })
      let promise = httpRequest.requestInStream(request.loadSrc as string, {
        header: headerObj,
        method: http.RequestMethod.GET,
        expectDataType: http.HttpDataType.ARRAY_BUFFER,
        connectTimeout: 60000, // 可选 默认60000ms
        readTimeout: 0, // 可选, 默认为60000ms
        usingProtocol: http.HttpProtocol.HTTP1_1, // 可选,协议类型默认值由系统自动指定
        usingCache: false
      });
      await promise.then((data) => {
        if (data == 200) {

        } else {
          onError(`HttpDownloadClient has error, http code = ` + JSON.stringify(data))
        }
      }).catch((err: Error) => {
        onError(`HttpDownloadClient has error, http code = ` + JSON.stringify(err))
      })
    } catch (err) {
      onError('HttpDownloadClient catch err request uuid =' + request.uuid)
    }
  }

  combineArrayBuffers(arrayBuffers: ArrayBuffer[]): ArrayBuffer {
    // 计算多个ArrayBuffer的总字节大小
    let totalByteLength = 0;
    for (const arrayBuffer of arrayBuffers) {
      totalByteLength += arrayBuffer.byteLength;
    }

    // 创建一个新的ArrayBuffer
    const combinedArrayBuffer = new ArrayBuffer(totalByteLength);

    // 创建一个Uint8Array来操作新的ArrayBuffer
    const combinedUint8Array = new Uint8Array(combinedArrayBuffer);

    // 依次复制每个ArrayBuffer的内容到新的ArrayBuffer中
    let offset = 0;
    for (const arrayBuffer of arrayBuffers) {
      const sourceUint8Array = new Uint8Array(arrayBuffer);
      combinedUint8Array.set(sourceUint8Array, offset);
      offset += sourceUint8Array.length;
    }

    return combinedArrayBuffer;
  }
}
```

## 4、优化成果

|  | 平均耗时                | max耗时       | min耗时 |
|  -------  |---------------------|-------------|-------|
| 优化前 | 154ms 115μs 357.4ns | 832ms 868μs | 728μs |
| 优化后 | 104ms 538μs 164.2ns | 587ms 5μs   | 853μs |
