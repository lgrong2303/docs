# ImageKnife性能总结-OnAreaChange优化

## 1、背景
WeLink、微博在定位性能问题时，发现ImageKnife加载图片时大量监听了OnAreaChange接口，该对系统资源消耗较大，导致应用偶发性丢帧。

## 2、原因分析
1、ImageKnife为了获取组件宽高，进行图片解码，监听了OnAreaChange方法。

2、由于系统机制每次触发Vsync渲染都会回调OnAreaChange，引起不必要的资源消耗。

代码参考：
```shell
.onAreaChange((oldValue: Area, newValue: Area) => {
   if(newValue != undefined && newValue.width != undefined && newValue.height != undefined) {
     this.currentWidth = newValue.width as number
     this.currentHeight = newValue.height as number
     if (this.currentWidth <= 0 || this.currentHeight <= 0) {
       // 存在宽或者高为0,此次重回无意义,无需进行request请求
     } else {
       // 前提：宽高值均有效,值>0. 条件1：当前宽高与上一次宽高不同  条件2:当前是第一次绘制
       if ((this.currentHeight != this.lastHeight || this.currentWidth != this.lastWidth) || this.firstDrawFlag) {
         this.firstDrawFlag = false;
         this.lastWidth = this.currentWidth
         this.lastHeight = this.currentHeight
         this.imageKnifeExecute()
       }
     }
   } else {
     LogUtil.log('ImageKnifeComponent onAreaChange Error newValue is undefined')
   }
})
```

## 3、优化方案
在inspector回调时主动调用componentUtils.getRectangleById方法获取宽高。

代码参考：
```shell
 onLayoutComplete:()=>void = ():void=>{
    this.value = componentUtils.getRectangleById(this.keyCanvas.keyId);
    this.currentWidth = px2vp(this.value.size.width)
    this.currentHeight = px2vp(this.value.size.height)
    if (this.currentWidth <= 0 || this.currentHeight <= 0) {
      // 存在宽或者高为0,此次重回无意义,无需进行request请求
    } else {
      // 前提：宽高值均有效,值>0. 条件1：当前宽高与上一次宽高不同  条件2:当前是第一次绘制
      if ((this.currentHeight != this.lastHeight || this.currentWidth != this.lastWidth) || this.firstDrawFlag) {
        this.firstDrawFlag = false;
        this.lastWidth = this.currentWidth
        this.lastHeight = this.currentHeight
        this.imageKnifeExecute()
      }
    }
  }
```

## 4、优化成果

|  | 图片加载速度平均耗时      | 相同时间CPU指令个数 | 
|  -------  |-----------------|-------------
| 优化前 | 2ms 915μs 476ns | 3925183432  |
| 优化后 | 2ms 56μs 708ns  | 2746425471  |