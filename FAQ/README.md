# Js/Ts/ArkTs三方库常见FAQ

## 一、ImageKnife

- [ImageKnife性能总结-网络优化.md](ImageKnife%2FImageKnife%E6%80%A7%E8%83%BD%E6%80%BB%E7%BB%93-%E7%BD%91%E7%BB%9C%E4%BC%98%E5%8C%96.md)

- [ImageKnife性能总结-OnAreaChange优化.md](ImageKnife%2FImageKnife%E6%80%A7%E8%83%BD%E6%80%BB%E7%BB%93-OnAreaChange%E4%BC%98%E5%8C%96.md)


## 二、ohos-MPChart

- [鸿蒙MPChart图表自定义（一）绘制一条虚实相接的曲线.md](ohos-MPChart%2F%E9%B8%BF%E8%92%99MPChart%E5%9B%BE%E8%A1%A8%E8%87%AA%E5%AE%9A%E4%B9%89%EF%BC%88%E4%B8%80%EF%BC%89%E7%BB%98%E5%88%B6%E4%B8%80%E6%9D%A1%E8%99%9A%E5%AE%9E%E7%9B%B8%E6%8E%A5%E7%9A%84%E6%9B%B2%E7%BA%BF.md)

- [鸿蒙MPChart图表自定义（二）根据y轴刻度绘制渐变色曲线.md](ohos-MPChart%2F%E9%B8%BF%E8%92%99MPChart%E5%9B%BE%E8%A1%A8%E8%87%AA%E5%AE%9A%E4%B9%89%EF%BC%88%E4%BA%8C%EF%BC%89%E6%A0%B9%E6%8D%AEy%E8%BD%B4%E5%88%BB%E5%BA%A6%E7%BB%98%E5%88%B6%E6%B8%90%E5%8F%98%E8%89%B2%E6%9B%B2%E7%BA%BF.md)

- [鸿蒙图表MPChart自定义样式（五）左y轴显示数值，右y轴显示百分比.md](ohos-MPChart%2F%E9%B8%BF%E8%92%99%E5%9B%BE%E8%A1%A8MPChart%E8%87%AA%E5%AE%9A%E4%B9%89%E6%A0%B7%E5%BC%8F%EF%BC%88%E4%BA%94%EF%BC%89%E5%B7%A6y%E8%BD%B4%E6%98%BE%E7%A4%BA%E6%95%B0%E5%80%BC%EF%BC%8C%E5%8F%B3y%E8%BD%B4%E6%98%BE%E7%A4%BA%E7%99%BE%E5%88%86%E6%AF%94.md)


