# OpenHarmony npm包

OpenHarmony js/ts三方库使用的是OpenHarmony npm包，它是在传统的npm包的基础上，定义了OpenHarmony npm共享包特定的工程结构和配置文件，支持OpenHarmony页面组件相关API、资源的调用。通过OpenHarmony npm包，您可以实现多个模块或者多个工程共享OpenHarmony页面、资源等相关代码。

OpenHarmony npm共享包的实现依赖于npm，因此您需要了解和掌握npm的基础功能和机制，可通过[npm官方文档](https://docs.npmjs.com/about-npm)进行了解

# 如何安装OpenHarmony npm包
a. 设置 OpenHarmony推荐的npm专用仓库（如果使用DevEco Studio 3.0 Beta3及以上版本的命令行窗口，则可忽略此步骤）
```
npm config set @ohos:registry=https://repo.harmonyos.com/npm/
```
b.在命令行工具中，执行如下命令进行安装，如安装vcard三方库，依赖包会存储在工程的node_modules目录下@ohos\vcard下。
```
npm install @ohos/vcard --save
```
在package.json中会自动添加如下依赖：
```
"dependencies": {
  "@ohos/vcard": "^2.0.5"
}
```
