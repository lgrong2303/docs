# OpenHarmony npm Package

OpenHarmony JS/TS third-party libraries use OpenHarmony npm packages. Based on the traditional npm packages, OpenHarmony defines the specific project structure and configuration file of the OpenHarmony npm shared package, which supports the invocation of APIs and resources related to OpenHarmony UI components. The OpenHarmony npm package enables multiple modules or projects to share code related to OpenHarmony UI and resources.

The implementation of the OpenHarmony npm shared package depends on npm. You can go to the [official npm website](https://docs.npmjs.com/about-npm) to learn about basic functions and mechanisms of npm.

# Installing the OpenHarmony npm Package
a. Set the npm repository recommended by OpenHarmony. (Skip this step if the CLI of DevEco Studio 3.0 Beta3 or later is used.)
```
npm config set @ohos:registry=https://repo.harmonyos.com/npm/
```
b. In the CLI, run the following command to install the third-party library. For example, if the third-party library **vcard** is installed, the dependency package is stored in **node_modules** > **@ohos\vcard** of the project.
```
npm install @ohos/vcard --save
```
The following dependency is automatically added to the **package.json** file:
```
"dependencies": {
  "@ohos/vcard": "^2.0.5"
}
```
