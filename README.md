# docs

## 介绍

docs是OpenHarmony三方组件库私有仓组织的一个文档仓，存放OpenHarmony三方组件子项目文档。

## OpenHarmony-TPC 开发者文档

### 开发者文档

- [如何创建新的三方库](./contribute/%E5%88%9B%E5%BB%BA%E4%B8%89%E6%96%B9%E5%BA%93%E5%B7%A5%E7%A8%8B%E6%8C%87%E5%AF%BC.md)
- [如何移植js三方库](./contribute/adapter-guide/js%E7%A7%BB%E6%A4%8D%E9%80%82%E9%85%8D%E6%8C%87%E5%AF%BC.md)
- [如何移植c/c++三方库](./contribute/adapter-guide/c_c%2B%2B%E7%A7%BB%E6%A4%8D%E9%80%82%E9%85%8D%E6%8C%87%E5%AF%BC.md)
- [har包使用指导](./OpenHarmony_har_usage.md)

### 贡献者参考文档

- [贡献三方库到OpenHarmony三方库中心仓](./contribute/ohpm%E5%8F%91%E5%B8%83%E6%8C%87%E5%AF%BC.md)
- [贡献三方库到Openharmony-TPC](./contribute/TPC%E5%85%B1%E5%BB%BA%E6%8C%87%E5%AF%BC.md)
- [OpenHarmony三方库发布标准](./contribute/TPC%E5%8F%91%E5%B8%83%E6%A0%87%E5%87%86.md)
- [OpenHarmony-TPC开源合规规范](./contribute/rules/OpenHarmony-TPC%E5%BC%80%E6%BA%90%E5%90%88%E8%A7%84%E8%A7%84%E8%8C%83.md)

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/docs/blob/master/LICENSE) ，请自由地享受和参与开源。
