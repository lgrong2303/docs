# OpenHarmony-TPC 开源三方库贡献

## 简介

开发者贡献一个三方库到[OpenHarmony-TPC](https://gitee.com/openharmony-tpc)的流程如下：

![共建流程](../imgs/tpc_contribute_process.png)

从以上流程可知道，在整个过程中，共建者需要进行sig建仓申请，门禁申请以及sig孵化毕业申请等操作。本文详细介绍几个申请的具体步骤。

## OpenHarmony-sig建仓

- 建仓申请

  在OpenHarmony 架构SIG会议上申报议题，议题内容是申请建三方库仓。上会时需要准备建仓申请材料，会议上需要说明 三方库的背景，能力，价值以及选型。建仓会议申请请参照文档[sig管理](https://gitee.com/openharmony/community/blob/master/zh/sig_governance.md#21--openharmony-%E9%A1%B9%E7%9B%AE%E4%BB%93%E5%BA%93%E7%AE%A1%E7%90%86%E6%B5%81%E7%A8%8B)

- 创建三方库SIG仓
  
  1. [登录OpenHarmony数字化平台](http://ci.openharmony.cn/workbench/cicd/codecontrol/list) <br>
    &nbsp;![登录](../imgs/ci_login1.png)
    &nbsp;![登录](../imgs/ci_login2.png)
  2. 选SIG管理下的SIG仓申请     <br>
    建仓具体步骤如下图所示：    <br>
    &nbsp;![建仓步骤](../imgs/warehouse_create_step.png)
  3. SIG建仓内容填写    <br>
    &nbsp;![建仓内容填写](../imgs/warehouse_infomations.png)  <br>
    ***建仓申请通过了构架SIG评审后，会议纪要会上传到:https://lists.openatom.io/hyperkitty/list/dev@openharmony.io/, 通过该网址按时间搜索到具体的纪要信息***
  4. 填写完后后提交申请，可以通过`审批人`一栏联系当前的审批人进行审批，审批通过后仓库自动建成。

## 门禁配置

  门禁是保证代码质量是否达到质量要求关键手段之一，其主要包含代码编译、静态/安全/开源检查、敏感词/copyright扫描等。  <br>
  门禁申请的主要步骤如下：

  1. 在OpenHarmony-sig的[manifest仓](https://gitee.com/openharmony-sig/manifest)创建对应三方仓的xml配置文件。
  2. 登录[OpenHarmony数字化平台](http://ci.openharmony.cn/workbench/cicd/codecontrol/list)，在SIG管理下选择流水线申请,具体如下图所示:   <br>
    &nbsp;![门禁内容](../imgs/pipeline_process_info1.png)
    &nbsp;![门禁内容](../imgs/pipeline_process_info2.png)

    门禁所需填写的内容如上图所示，其余部分使用默认内容即可。OpenHarmony-TPC开源三方库当前预编译命令:

    ```shell
    rm -rf cicd && git clone https://gitee.com/openharmony-sig/cicd.git -b tpc_compile && chmod 777 cicd/js/* && ./cicd/js/prebuild.sh ${PR_URL}
    ```

    编译命令:

    ```shell
    ./cicd/js/build.sh ${PR_URL}
    ```

  3. 流水线审批
    流水线提交申请后可以通过`当前处理人`一栏查看审批人员，可以联系审批人员对申请进行处理。

  流水线审批通过后，仓库的门禁就可以正常运行。

## OpenHarmony-sig孵化毕业

当三方库功能完成且符合[sig孵化毕业要求](https://gitee.com/openharmony/community/blob/master/sig/sig_qa/guidance_for_incubation_project_graduation_cn.md#sig%E5%AD%B5%E5%8C%96%E9%A1%B9%E7%9B%AE%E6%AF%95%E4%B8%9A%E8%AF%84%E5%AE%A1%E6%A3%80%E6%9F%A5%E9%A1%B9)后，可以申请申请孵化毕业，将该三方库孵化到OpenHarmony-TPC社区。
孵化毕业的流程主要包含：

- 孵化预审 <br>
  孵化预审会议定在每周二上午，孵化预审的步骤：
  1. [申请议题](https://gitee.com/openharmony/community/blob/master/zh/sig_governance.md#232-%E7%94%B3%E8%AF%B7%E8%B4%A8%E9%87%8Fsig%E5%AD%B5%E5%8C%96%E5%87%86%E5%87%BA%E8%AF%84%E5%AE%A1)。每次议题需要在上一周周日前申报完。
  2. 申请完议题后需准备好预审材料(其中需要注意当时建仓时是否有遗留问题，此次评审需要保证之前的预留问题已解决)，[预审材料模板](https://gitee.com/openharmony/community/blob/master/sig/sig_architecture/meetings/repository_review_template.pptx)
- 准出评审 <br>
  孵化预审通过后，需要申请质量SIG孵化准出评审，该会议定在每周二下午。
  1. [申请议题](https://gitee.com/openharmony/community/blob/master/zh/sig_governance.md#23-%E4%BB%93%E5%BA%93%E5%AD%B5%E5%8C%96%E5%87%86%E5%87%BA)。
  2. 参考[准出标准](https://gitee.com/openharmony/community/blob/master/sig/sig_qa/guidance_for_incubation_project_graduation_cn.md#sig%E5%AD%B5%E5%8C%96%E9%A1%B9%E7%9B%AE%E6%AF%95%E4%B8%9A%E8%AF%84%E5%AE%A1%E6%A3%80%E6%9F%A5%E9%A1%B9)准备孵化准出材料。
  3. 涉及`代码检查报告`、`OAT扫描报告`、`Foss license扫描报告`、`资料规范性扫描报告`、`漏洞扫描报告`、`病毒扫描报告`、`XTS测试报告`等优先通过OpenHarmony数字化平台的孵化准出流程中获取，如若获取失败在通过其他方法获取。

- CI 平台孵化准出申请 <br>
   准出评审通过后，可以在[登录OpenHarmony数字化平台](http://ci.openharmony.cn/workbench/cicd/codecontrol/list)提交孵化准出电子流，具体步骤如下：
   1. [登录OpenHarmony数字化平台](http://ci.openharmony.cn/workbench/cicd/codecontrol/list)
   2. 选择孵化准出

      &nbsp;![选择孵化准出](../imgs/incubate_step.png)

   3. 填写孵化准出内容

      &nbsp;![孵化准出内容1](../imgs/incubate_content1.png)
      &nbsp;![孵化准出内容2](../imgs/incubate_content.png)

## 合入OpenHarmony-TPC

三方仓孵化准出成功后自动合入到OpenHarmony-TPC。

## 参考资料
