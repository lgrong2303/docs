# 发布OpenHarmony三方库中心仓
## 检查三方库内容中可能涉及的敏感信息
将敏感信息发布到 OpenHarmony 三方库中心仓可能会损害您的用户、危及您的开发基础架构、修复成本高昂，并使您面临法律诉讼的风险。我们强烈建议在将三方库发布到 OpenHarmony 三方库中心仓之前，删除敏感信息，例如私钥、密码、个人信息和信用卡数据等。

## 发布准备
1. 在进行 publish 发布前，请先确保在[OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)上已经创建了帐号，且进行了[安全设置](https://ohpm.openharmony.cn/#/cn/help/personalset)。

![img](../imgs/ohpm_message.png)

2. 登录OpenHarmony三方库中心仓，从 【个人中心】页面中【复制发布码】，并配置到 .ohpmrc 文件中 publish_id 字段上，可执行如下命令：

```
ohpm config set publish_id your_publish_id
```

## 发布
执行如下命令发布HAR，<HAR路径>请指定为待发布HAR的具体路径
```
ohpm publish <HAR路径>
```
publish 命令其他使用方法及相关规则，请参阅 ohpm publish 常用命令章节。

发布成功之后，ohpm 将会给您的账户发送“创建上架审核单成功”通知，您可登录OpenHarmony三方库中心仓，进入个人中心界面，关注【消息】通知。

创建上架审核单成功通知消息示例：

## 等待审核
审核通过之后将会给您的账户发送 “审核通过”通知，您可登录OpenHarmony三方库中心仓个人中心管理界面，关注【个人中心】-【消息】通知。

上架审核通过通知消息示例：

![img](../imgs/ohpm_new.png)

## 管理已发布的三方库
登录OpenHarmony三方库中心仓，在【个人中心】-【Package】管理界面，查看已发布的三方库审核、上下架状态。

![img](../imgs/ohpm_package.png)

## 【可选】将三方库发布到社区“三方库资源汇总”

将三方库按照库分类三放到[三方库资源汇总](https://gitee.com/openharmony-tpc/tpc_resource)的社区共建下。
