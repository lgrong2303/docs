# 创建新的三方库
创建OpenHarmony三方库，建议使用[Deveco Studio](https://developer.harmonyos.com/cn/develop/deveco-studio/)，并添加ohpm工具的环境变量到PATH环境变量。

## 创建
### 方法1：IDE界面创建
在现有应用工程中，新创建Module，选择"Static Library"模板，创建完成后，完善
oh-package.json5的信息，其中包名称、版本等包信息根据实际情况填写。

![img](../imgs/create_byide.png)


### 方法2：ohpm命令行创建
执行ohpm init命令创建包信息文件oh-package.json5，其中包名称、版本等包信息根据实际情况填写。
![img](../imgs/create_byconsole.png)

## 完善oh-package.json5文件
将三方库发布到 OpenHarmony 三方库中心仓，必须包含 oh-package.json5 文件，其是对当前三方库的元数据描述。

一个 oh-package.json5 文件：
- 列出项目中所依赖的三方库
- 使用 semver 规范指定项目中可以使用的三方库版本

oh-package.json5 字段说明：
![img](../imgs/oh-package.json5.png)

## 编译打包HAR
开发完库模块后，选中模块名，然后通过DevEco Studio菜单栏的Build > Make Module ${libraryName}进行编译构建，生成HAR。HAR可用于工程其它模块的引用，或将HAR上传至ohpm仓库，供其他开发者下载使用。若部分源码文件不需要打包至HAR/HSP中，可通过创建.ohpmignore文件，配置打包时要忽略的文件/文件夹。
