# 移植js三方库

## 第一步：js/ts库选型

由于OpenHarmony应用是基于ArkTS开发，因此在开发OpenHarmony三方库时，建议首选在成熟的js/ts开源三方库上开发。通常方法是在github/npm上按照特性和语言搜索，找到star,fork数量较高的，且开源协议友好的js/ts开源三方库。

## 第二步：工具扫描三方库，检查是否依赖node.js/web内置模块

由于OpenHarmony开发框架里的API不完全兼容V8运行时的Build-In API，因此建议在使用js三方库前，使用[js-e2e工具](https://gitee.com/chenwenjiehw/js-e2e/tree/master/js-compatibiitiy)扫描三方库，检查是否存在node.js/web内置模块的依赖。js-e2e工具是基于eslint进行封装，可分析出JS库代码对node.js和web浏览器的内置模块、对象的依赖及兼容ES标准版本，使用该工具，可以快速知道该库是否依赖node.js或者web内置模块。
如果扫描结果不依赖node.js/web内置模块，恭喜你，这个库将会比较轻松地移植。如果大量依赖nodejs/web库，那可能需要fork源库代码，进行侵入式修改，或者是再找下是否有更合适地其他三方库。
注意，扫描的时候也需要同时扫描下package.json里面dependencies下的直接依赖和间接依赖，因为发布OpenHarmony三方库中心仓时，需要依赖的组件都发布。建议使用npm install下载所有依赖的代码后，一起扫描看结果。

## 第三步：跑通xts用例，验证所有对外暴露接口可用

非UI的库，通常使用xts用例验证和保障对外暴露的接口在OpenHarmony上可用。通常建议把这个三方库的源码copy到DevEco Studio中，采用源库的单元测试用例，连接OpenHarmony系统的rom设备，跑单元测试，来验证该库是否能在OpenHarmony系统100%运行。

其中把源码拷贝到src/main/ets下，测试代码拷贝到src/ohosTest/ets下。

在Ability.test.ets文件中导入源库的测试用例。

由于JS或者TS断言接口与OpenHarmony系统提供的断言接口不一致，所以，在源库的测试用例上，需要手动更改下断言。下面提供下OpenHarmony断言的接口列表：

|  接口名   | 描述  |
|  ----  | ----  |
| assertClose  | 检验actualvalue和expectvalue(0)的接近程度是否是expectValue(1) |
| aassertContain    | 检验actualvalue中是否包含expectvalue |
| aassertEqual    | 检验actualvalue是否等于expectvalue[0] |
| aassertFail    | 抛出一个错误 |
| aassertFalse    | 检验actualvalue是否是false |
| aassertTrue    | 检验actualvalue是否是true |
| aassertInstanceOf    | 检验actualvalue是否是expectvalue类型 |
| aassertLarger    | 检验actualvalue是否大于expectvalue |
| aassertLess    | 检验actualvalue是否小于expectvalue |
| aassertNull    | 检验actualvalue是否是null |
| aassertThrowError    | 检验actualvalue抛出Error内容是否是expectValue |
| aassertUndefined    | 检验actualvalue是否是undefined |

可以参考diff库的单元[测试用例](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jsDiffDemo/entry/src/ohosTest/ets/test)：

根据对应的断言，运行测试用例，看源库的单元测试用例是否能通过。如果通过，则可以大概率该库在OpenHarmony系统下能运行。

如果不通过，可以通过以下几个方面进行排查并解决问题:  
1.检查测试代码的接口调用是否使用正确。

2.如果是nodejs/web内置模块问题，则需要使用OpenHarmony SDK替代对应的系统方法。

3.如果是系统限制，如不支持动态函数,eval等语法，则需要更换实现。

4.如果是系统接口差异，则建议在OpenHarmony gitee仓下提issue或者在OpenHarmony开发者论坛上描述问题求助。

## 第四步：规范源码目录结构，开源代码到gitee/github

参考axios三方库的目录，规范源码目录结构。

```shell
|---- axios三方库名称 
|     |---- AppScrope                   # 示例代码文件夹
|     |---- entry                       # 示例代码文件夹 
|     |---- hvigor                      # 项目配置  
|     |---- screenshot                  # 效果截图
|     |---- lottieArkTS                 # 项目配置 
|     |---- axios                       # axios三方库文件夹(一般为库名) 
|           |---- build                 # axios模块打包后的文件 
|           |---- src                   # 模块代码 
|                |---- ets/components   # 模块代码 
|                     |---- lib         # axios 网络请求核心代码 
|                     |---- sandbox     # axios 网络请求sample代码 
|                     |---- templates   # axios 网络请求核心代码 
|                     |---- index.d.ts  # axios 接口类型定义 
|            |---- index.js             # 入口文件 
|            |---- index.d.ts           # 声明文件 
|            |---- *.json5              # 配置文件 
|     |---- README.md                   #  需要包括功能介绍，使用方法，API说明， 约束与限制
|     |---- README.OpenSource           # 开源说明 
|     |---- CHANGELOG.md                # 更新日志
```
