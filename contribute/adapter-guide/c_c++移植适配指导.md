# 移植c/c++三方库

如果没有合适的js/ts三方库，此时可以查找是否有成熟的C/C++三方库可以移植到OpenHarmony中，目前可以在这里查看其他开发者已验证可在OpenHarmony上编译通过的C/C++三方库。
通常相对于JS/TS的三方组件来说，C/C++三方库运行效率更高，但首先要能完成在OpenHarmony编译环境下的构建，而且需要再封装NAPI或者使用@ohos/aki对应用提供js接口。接下来将介绍如何快速高效的移植一个C/C++三方库到OpenHarmony上。

C/C++三方库适配问题与解决方案

![img](../../imgs/c_migrate.png)

由上图可以了解到，C/C++三方库移植的流程、关键阻塞点和解决方法，接下来我们详细介绍具体的步骤。

## 第一步：三方库运行时依赖分析

针对运行时依赖的分析，我们开发了对应的[c/c++风险识别工具](https://gitee.com/han_jin_fei/e2e/tree/master/thirdparty_compare)，通过该工具可以扫描出三方库是否有对NDK，OpenGL等接口的依赖，以及是否有bionic的C库接口的依赖等。该工具可以让我们快速的对一个C/C++三方库进行风险识别。

如果依赖无法兼容的接口则该库无法移植，建议更换其他库；如果不依赖以上接口，仍需要继续验证。注意：同时需要分析依赖库的源码。

## 第二步：三方库编译构建

OpenHarmony的应用编译开发使用的是[Deveco Studio](https://developer.harmonyos.com/cn/develop/deveco-studio#download)，而该工具目前只支持cmake的编译，但开源的C/C++三方库编译方式多样化，包含cmake，configured等方式。对于原生库cmake无法在IDE上编译构建的，我们需要分析问题原因并需要针对IDE修改原生CMakeLists.txt。而非cmake编译方式的三方库，我们也需要分析该库的编译方式进行手写CMakeLists.txt文件将该库编译方式改为cmake编译构建。这些过程比较耗时，尤其对一些大型的C/C++三方库。

针对于这些问题，我们开发一套基于linux下用原生库的编译脚本进行交叉编译三方库的工具[lycium](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/lycium)。该工具协助开发者，在 linux系统上快速编译构建能在OpenHamony上运行的c/c++ 三方库。

使用lycium工具编译开源库，在lycium工具目录会生成usr目录，里面是三方库头文件及生成的库，该目录下存在已编译完成的32位和64位三方库。需要注意的是：
●有依赖库的必须将依赖库一起编译，否则框架无法进行编译。
●安装目录下对应的三方库有2份，其中armeabi-v7a对应的32位的库，arm64-v8a对应的是64位的库。
接下来就可以对三方库进行测试验证了。

## 第三步：三方库测试验证

业界内C/C++三方库测试框架多种多样，我们无法将其统一，因此为了保证原生库功能完整，我们基于原生库的测试用例进行测试验证。为此，我们集成了一套可以在OH环境上进行make test等操作的环境:[CITools](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/lycium/CItools/README_zh.md) ，通过配置完这套环境，我们就可以在OH环境上运行原生库的测试用例了。

## 第四步：三方库的使用

验证通过后，就可以使用三方库了。将三方库二进制拷贝到对应的工程`\\entry\libs\armxx\`目录，头文件拷贝到工程`\\entry\src\main\cpp`目录下。
可参考curl集成到应用hap，了解三方库的使用：

- 拷贝动态库到`\\entry\libs\${OHOS_ARCH}\`目录： 动态库需要在`\\entry\libs\${OHOS_ARCH}\`目录，才能集成到hap包中，所以需要将对应的so文件拷贝到对应CPU架构的目录

![img](../../imgs/c_lib.png)

- 在IDE的cpp目录下新增thirdparty目录，将编译生成的库拷贝到该目录下，如下图所示  

![img](../../imgs/c_include.png)

- 在最外层（cpp目录下）CMakeLists.txt中添加将将三方库和三方库的头文件和加入工程中语句。

  ```cmake
  target_link_libraries(entry PRIVATE ${CMAKE_SOURCE_DIR}/../../../libs/${OHOS_ARCH}/libcurl.so)                # 将三方库加入工程中
  target_include_directories(entry PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/curl/${OHOS_ARCH}/include)    # 将三方库的头文件加入工程中
  ```

接下来就可以使用三方库了。

## 第五步：开发js/ts接口

提供NAPI或者使用@ohos/aki，对应用提供js/ts接口。

## 第六步：规范源码目录

参考[commons-compress](https://gitee.com/openharmony-tpc/CommonsCompress)三方库的目录，规范源码目录结构，把三方库的源码或二进制引入到cpp目录，在ets目录中引入js api。

```shell
/commons-compress               # 三方库源代码
├── src                         # 框架代码
│   └── main
│       └── cpp 
│           ├── zstd            # zstd C源码目录
│           └── zstd.cpp        # zstd Napi封装接口
│       └── ets
│           └── components
│               └── archivers
│                   ├── ar      # ar源代码存放目录
│                   ├── cpio    # cpio源代码存放目录
│                   ├── dump    # dump源代码存放目录
│                   ├── lzma    # lzma源代码存放目录
│                   ├── tar     # tar源代码存放目录
│                   └── zip     # zip源代码存放目录
│               └── compressors
│                   ├── brotli  # brotli源代码存放目录
│                   ├── bzip2   # bzip2源代码存放目录
│                   ├── lz77support  # lz77support源代码存放目录
│                   ├── lzw     # lzw源代码存放目录
│                   ├── snappy  # snappy源代码存放目录
│                   ├── xz      # xz源代码存放目录
│                   └── z       # z源代码存放目录
│               ├── deflate     # deflate源代码存放目录
│               ├── gzip        # gzip源代码存放目录
│               ├── lz4         # lz4源代码存放目录
│               ├── util        # 工具源代码存放目录
│               └── zstd        # zstd源代码存放目录
```
