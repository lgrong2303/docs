# OpenHarmony三方库发布准则

## 简介

OpenHarmony-TPC下的三方库共建完后可以发布到[OpenHarmony中心仓](https://ohpm.openharmony.cn/#/cn/home)。发布后，可以在中心仓网站个人消息处查看发布的结果。如果审核通过后，也可以在中心仓直接搜索到这个三方库。

## 发布准则

- 发布的三方库必须遵循以下规则

1. 发布的三方库必须有明确的功能。 <br>
2. 发布的三方库库命名需规范。 <br>
   请参考[OpenHarmony 三方库名称指南](https://ohpm.openharmony.cn/#/cn/help/guidename)。
3. 发布的三方库需包含Readme文件。     <br>
   建议ReadMe中包含简介，下载安装，所需权限，使用示例，接口说明，约束与限制(支持API几，SDK版本)，目录结构，贡献代码，开源协议。  之所以有这些要求，也是便于开发者使用。
4. 发布的三方库必须包含License 文件。
5. 发布的三方库需包含ChangeLog文件。
6. 发布的三方库版本命名需规范。
   请参照[OpenHarmony-TPC版本命名指导](./rules/OpenHarmony-TPC%E7%89%88%E6%9C%AC%E5%91%BD%E5%90%8D%E6%8C%87%E5%AF%BC.md)。
