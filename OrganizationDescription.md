# OpenHarmony-TPC

# 介绍

OpenHarmony-TPC（Third Party Components）用于汇集OpenHarmony系统上的开源三方库，帮助应用开发者快速开发OpenHarmony应用。

OpenHarmony-TPC由OpenHarmony知识体系工作组下属的OpenHarmony三方库SIG主导，欢迎您使用及参与贡献。

# OpenHarmony-TPC 三方库

- [三方库资源汇总](https://gitee.com/openharmony-tpc/tpc_resource)
- [OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

# 贡献OpenHarmony三方库

- [创建新的三方库](./contribute/%E5%88%9B%E5%BB%BA%E4%B8%89%E6%96%B9%E5%BA%93%E5%B7%A5%E7%A8%8B%E6%8C%87%E5%AF%BC.md)
- [移植js三方库](./contribute/adapter-guide/js%E7%A7%BB%E6%A4%8D%E9%80%82%E9%85%8D%E6%8C%87%E5%AF%BC.md)
- [移植c/c++三方库](./contribute/adapter-guide/c_c%2B%2B%E7%A7%BB%E6%A4%8D%E9%80%82%E9%85%8D%E6%8C%87%E5%AF%BC.md)
- [OpenHarmony三方库发布标准](./contribute/TPC%E5%8F%91%E5%B8%83%E6%A0%87%E5%87%86.md)
- [发布OpenHarmony三方库中心仓](./contribute/ohpm%E5%8F%91%E5%B8%83%E6%8C%87%E5%AF%BC.md)
- [贡献到Openharmony-TPC](./contribute/TPC%E5%85%B1%E5%BB%BA%E6%8C%87%E5%AF%BC.md)

# 治理规则

- [OpenHarmony-TPC开源合规规范](./contribute/rules/OpenHarmony-TPC%E5%BC%80%E6%BA%90%E5%90%88%E8%A7%84%E8%A7%84%E8%8C%83.md)
- [OpenHarmony-TPC版本命名规则](./contribute/rules/OpenHarmony-TPC%E7%89%88%E6%9C%AC%E5%91%BD%E5%90%8D%E6%8C%87%E5%AF%BC.md)
- [OpenHarmony-TPC漏洞治理规范](./contribute/rules/OpenHarmony-TPC%E6%BC%8F%E6%B4%9E%E6%B2%BB%E7%90%86%E8%A7%84%E8%8C%83.md)

# 组织会议

- 目前视任务进度情况决定开会时间
- 通过邮件申报议题(邮件列表暂未完善，请先发送至xxx)

# Committer

- [@MaDiXin](https://gitee.com/MaDiXin)
- [@xiafeng_xf_admin](https://gitee.com/xiafeng_xf_admin)
- [@金永生](https://gitee.com/Cruise2019)
- [@zeeman_wang](https://gitee.com/zeeman_wang)

# 社区交流

- 微信群：待补充
- 邮件列表: 待补充
